public class SimpleWar{
	public static void main(String[] args){
		Deck deck = new Deck();
		deck.shuffle();
		// System.out.println(deck);
		double player1Points = 0;
		double player2Points = 0;
		
		//For testing
		// System.out.println(deck.drawTopCard());
		// System.out.println(deck.drawTopCard());
		// System.out.println(deck.drawTopCard().calculateScore());
		
		while(deck.length() > 0){
			//Prints cards
			Card card1 = deck.drawTopCard();
			Card card2 = deck.drawTopCard();
			System.out.println(card1);
			System.out.println(card2);
			//Prints scores
			double card1Score = card1.calculateScore();
			double card2Score = card2.calculateScore();
			System.out.println("Scores: \n" + "Player 1: " + card1Score);
			System.out.println("Player 2: " + card2Score + "\n");
			
			//Winner
			if(card1Score > card2Score){
				System.out.println("Player 1 wins the round");
				player1Points += 1;
			} else{
				System.out.println("Player 2 wins wins the round");
				player2Points += 1;
			}
			
			System.out.println(player1Points);
			System.out.println(player2Points + "\n");
		}
		
		if(player1Points > player2Points){
			System.out.println("Player 1 wins the game with " + player1Points);
		}
		if(player2Points > player1Points){
			System.out.println("Player 2 wins the game with " + player2Points);
		} 
		if(player1Points == player2Points){
			System.out.println("Tie!");
		}
	}
}