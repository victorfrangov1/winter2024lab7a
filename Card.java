public class Card{
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public String toString(){
		String value = "";
		if(this.value == "1"){
			value = "A";
		}
		else if(this.value == "11"){
			value = "J";
		}
		else if(this.value == "12"){
			value = "Q";
		}
		else if(this.value == "13"){
			value = "K";
		}
		else{
			value = this.value;
		}
		return value + " of " + this.suit;
	}
	
	public double calculateScore(){
		double num = Double.parseDouble(this.value);
		Math.floor(num);
		switch(this.suit){
			case "hearts":
				num+=0.4;
				break;
			case "spades":
				num+=0.1;
				break;
			case "diamonds":
				num+=0.3;
				break;
			case "clubs":
				num+=0.2;
				break;
		}
	return num;
	}
}